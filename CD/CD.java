package CD;

import java.util.ArrayList;
import java.util.List;

public class CD {
	private String name;
	public List<Song>songs;

	public CD(String name) {
		super();
		this.name = name;
		this.songs=new ArrayList<Song>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addTitle(Song songs) {
		this.songs.add(songs);
	}
	
}