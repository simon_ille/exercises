package CD;


public class Main {

	public static void main(String[] args) {
		Player p1 = new Player();
		CD c1 = new CD("1");
		DVD dvd1 = new DVD("1");
		Title t1 = new Title("Bluemountainstate");
		Song s1 = new Song("Crankthat");
		
		
		dvd1.addTitle(t1);

		c1.addTitle(s1);
		p1.addPlayable(t1);
		p1.addPlayable(s1);
		p1.playAll();
	}

}
