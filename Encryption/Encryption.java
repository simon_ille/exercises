package Encryption;

public class Encryption implements Encrypter {
	public int shifter;

	@Override
	public String encrypt(String data) {
		String result = "";

		for (int i = 0; i < data.length(); i++) {

			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii + shifter);
			
			if (ascii <= 122 && ascii >= 97 && shifted > 122) {
				shifted -= 26;
				result += shifted;
			} else if (ascii <= 90 && ascii >= 65 && shifted > 90) {
				shifted -= 26;
				result += shifted;

			} else {
				result += character;
			}
		}
		return result;
	}
	
	public Encryption(int shifter) {
		super();
		this.shifter = shifter;

	}	
	public Encryption() {
		super();
		this.shifter = 2;
	}

	@Override
	public String decrypt(String data) {
		String result = "";
		for (int i = 0; i < data.length(); i++) {

			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii - shifter);
			
			if (ascii <= 122 && ascii >= 97 && shifted < 97) {
				shifted += 26;
				result += shifted;
			} else if (ascii <= 90 && ascii >= 65 && shifted < 65) {
				shifted += 26;
				result += shifted;

			} else {
				result += character;
			}
		}
		return result;
	}
}
