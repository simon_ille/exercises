package Engine;

public class Car {
	
	private Engine useEngine;
	
	public Car(Engine useEngine) {
		super();
		this.useEngine = useEngine;
	}
	
	public void gas(int amount) {
		int newAmount = amount/2;
		this.useEngine.run(newAmount);
	}
	
	public void getSerial() {
		System.out.println("Mein Motor hat die Seriennummer: " + this.useEngine.getSerial());
	}
}
