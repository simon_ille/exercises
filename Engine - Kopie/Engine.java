package Engine;

public interface Engine {
	
	public void run(int amount);
	public int getSerial();
}
