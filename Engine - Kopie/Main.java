package Engine;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Car c1 = new Car(new TopDiesel(11111));
		Car c2 = new Car(new SuperFastGas(22222));
		
		
		c1.gas(50);
		c2.gas(60);
		
		c1.getSerial();
		c2.getSerial();
	}
	
}
