package ObserverPattern;

public class ChristmasTree implements Observable{
	private int id;
	
	public ChristmasTree(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void inform() {
		// TODO Auto-generated method stub
		System.out.println("Ich bin ChristmasTree nummer " + id + ".");
	}

}