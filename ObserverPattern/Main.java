package ObserverPattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TrafficLight tl1 = new TrafficLight(1);
		TrafficLight tl2 = new TrafficLight(2);
		ChristmasTree ct1 = new ChristmasTree(1);
		
		Sensor s1 = new Sensor();
		s1.addObservable(tl1);
		s1.addObservable(tl2);
		s1.addObservable(ct1);
		
		s1.informAll();
	}

}
