package ObserverPattern;

public interface Observer {
	public void informAll();
	
	public void addObservable(Observable observable);
}