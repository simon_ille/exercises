package TicTacToe;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TicTacToe {

	static int[][] board = new int[3][3];
	Scanner s1 = new Scanner(System.in);
	
	//printBoard();
	
	public void printBoard() {
		System.out.println("/---|---|---\\");
		System.out.println("| " + board[0][0] + " | " + board[0][1] + " | " + board[0][2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[1][0] + " | " + board[1][1] + " | " + board[1][2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[2][0] + " | " + board[2][1] + " | " + board[2][2] + " |");
		System.out.println("/---|---|---\\");
	}
	
	/*public void setSign() {
		String input = s1.next();
		String[] ScannerstorageArray = input.split(",");
	}*/
	
	public void setSign(boolean player1Turn) {
		
		String scannerStorage = s1.next();
		String[] scannerStorageArray = scannerStorage.split(",");
		int row = Integer.parseInt(scannerStorageArray[0]);
		int col = Integer.parseInt(scannerStorageArray[1]);
		
		if(player1Turn) { 
			board[row][col] = 1;
		} else {
			board[row][col] = 2;
		}
	}
	
	public boolean checkWinner() {
		
		boolean winner = false;
		
		if (board[0][0] == board[0][1] && board[0][0] == board[0][2] && board[0][0] != 0) {
			winner = true;
		}
		else if (board[1][0] == board[1][1] && board[1][0] == board[1][2] && board[1][0] != 0) {
			winner = true;
		}
		else if (board[2][0] == board[2][1] && board[2][0] == board[2][2] && board[2][0] != 0) {
			winner = true;
		}
		
		else if (board[0][0] == board[1][0] && board[0][0] == board[2][0] && board[0][0] != 0) {
			winner = true;
		}
		else if (board[0][1] == board[1][1] && board[0][1] == board[2][1] && board[0][1] != 0) {
			winner = true;
		}
		else if (board[0][2] == board[1][2] && board[0][2] == board[2][2] && board[0][2] != 0) {
			winner = true;
		}
		
		else if (board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] != 0) {
			winner = true;
		}
		else if (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] != 0) {
			winner = true;
		}
		return winner;
	}
}