package Treeschool;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private String areaName;
	private int areaSize;
	private List<Tree> trees;

	public Area(String areaName, int areaSize, List<Tree> trees) {
		super();
		this.areaName = areaName;
		this.areaSize = areaSize;
		this.trees = new ArrayList<Tree>();
	}

	public void doFertilize() {
		for (Tree tree : trees) {
			tree.getStrategy().doFertilize(areaName);
		}
	}
	
			public void addTree(Tree tree) {
			    trees.add(tree);
			  }

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public int getAreaSize() {
		return areaSize;
	}

	public void setAreaSize(int areaSize) {
		this.areaSize = areaSize;
	}

	public List<Tree> getTreeList() {
	    return trees;
	  }

	  public void setTreeList(List<Tree> treeList) {
	    this.trees = treeList;
	  }
}
