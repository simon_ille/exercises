package Treeschool;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		LeafTree lt1 = new LeafTree(3, 3, new Supergrow());
		LeafTree lt2 = new LeafTree(4, 4, new Supergrow());
		
		Nadelbaum nb1 = new Nadelbaum(4, 5, new Topgrow());
		Nadelbaum nb2 = new Nadelbaum(5, 6, new Topgrow());
		
		
		Area a = new Area("Area1", 50, null);
		a.addTree(lt1);
		a.addTree(lt2);
		a.addTree(nb1);
		a.addTree(nb2);
		
		a.doFertilize();
	}

}
