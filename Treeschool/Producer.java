package Treeschool;

public class Producer {
	  private String producerName;

	  public Producer(String producerName) {
	    super();
	    this.producerName = producerName;
	  }

	  public String getProducerName() {
	    return producerName;
	  }

	  public void setProducerName(String producerName) {
	    this.producerName = producerName;
	  }

	}