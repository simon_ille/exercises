package Treeschool;

public class Tree implements FertilizeStrategy {
	private int maxSize;
	private int maxDiameter;
	public FertilizeStrategy strategy;
	
	public Tree(int maxSize, int maxDiameter, FertilizeStrategy strategy) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.strategy = strategy;
	}

	public FertilizeStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(FertilizeStrategy strategy) {
		this.strategy = strategy;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

	@Override
	public void doFertilize(String strategy) {
		// TODO Auto-generated method stub
		
	}

}
