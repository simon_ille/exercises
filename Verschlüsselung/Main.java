package Verschlüsselung;

public class Encription implements Encripter {
	public int shifter;

	public Encription(int shifter) {
		super();
		this.shifter = shifter;

	}	
	public Encription() {
		super();
		this.shifter = 2;
	}

	@Override
	public String decript(String data) {
		String result = "";
		for (int i = 0; i < data.length(); i++) {

			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii - shifter);
			result += shifted;
		}
		return result;
	}

	@Override
	public String encript(String data) {
		String result = "";

		for (int i = 0; i < data.length(); i++) {

			char character = data.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii + shifter);

			result += shifted;
		}
		return result;
	}
}