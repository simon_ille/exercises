package VierGewinnt;

import java.util.Scanner;

public class VierGewinnt {
	Scanner s = new Scanner(System.in);
	
	static int rows = 6;
	static int columns = 8;
	char[][] board = new char[rows][columns];
	char player = 'X';
	boolean start = true;
	
	public void startGame() {
		initBoard();
		while (start) {
			printBoard();
			boolean isThereAWinner = checkWin();
			if (isThereAWinner) {
				start = false;
				printBoard();
				System.out.println("Super! Spieler '" + player + "' hat die Runde gewonnen!");
			}
		}
	}
	
	
	public void initBoard() {
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < columns; col++) {
				board[row][col] = ' ';
			}
		}
	}
	
	public char upperCaseWinner() {
		if (player == 'X') {
			return 'O';
		} else {
			return 'X';
		}
	}
	
	public boolean checkWin() {
		boolean winner = false;
		for (int row = 0; (!winner) && (row <= rows - 1); row++) {
			for (int col  = 0; (!winner) && (col < columns - 3); col++) {
					
				if (board[row][col] == player && board[row][col+1] == player && board[row][col+2] == player && board[row][col+3] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row][col+1] = upperCaseWinner();
					board[row][col+2] = upperCaseWinner();
					board[row][col+3] = upperCaseWinner();
				}
			}
		}
		
		for (int col = 0; (!winner) && (col <= columns - 1); col++) {

			for (int row  = 0; (!winner) && (row < rows - 3); row++) {
					
				if (board[row][col] == player && board[row+1][col] == player && board[row+2][col] == player && board[row+3][col] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row+1][col] = upperCaseWinner();
					board[row+2][col] = upperCaseWinner();
					board[row+3][col] = upperCaseWinner();
				}
			}
		}
		
		for (int col = 0; (!winner) && (col < columns - 3); col++) {
			for (int row  = rows - 1; row > rows - 3; row--) {
						
				if (board[row][col] == player && board[row-1][col+1] == player && board[row-2][col+2] == player && board[row-3][col+3] == player) {
					winner = true;
					board[row][col] = upperCaseWinner();
					board[row-1][col+1] = upperCaseWinner();
					board[row-2][col+2] = upperCaseWinner();
					board[row-3][col+3] = upperCaseWinner();
				}
			}
		}
		
		public void printBoard() {
			
			System.out.print("        ");
			for (int col = 1; col < columns; col++) {
				System.out.print("____");
			}
			System.out.print("____");
			System.out.println("");
			System.out.print("       /");
			for (int col = 1; col < columns; col++) {
				System.out.print("____");
			}
			System.out.println("___/|");
			System.out.print("       ");
			for (int col = 0; col < columns; col++) {
				System.out.print("|   ");
			}
			System.out.println("||");
			for (int row = 0; row < columns; row++) {
				System.out.print("       |");
				for (int col = 0; col < columns; col++) {
					
					System.out.print(" " + board[row][col] + " |");
				}
				System.out.println("|");
				if (row < (columns - 1)) {
					System.out.print("       |");
					for (int col = 1; col < columns; col++) {
						System.out.print("---|");
					}
					System.out.println("---||");
				}
			}
			System.out.print("       |");
			for (int col = 1; col < columns; col++) {
				System.out.print("___|");
			}
			System.out.println("___|/");
			System.out.println("");
			if(start) {
				System.out.println("Spieler '" + player + "' ist an der Reihe. In welches Feld willst du etwas schreiben?");
			}
		}
		

}